﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Join : MonoBehaviour
{

	public Color StandardColor;
	public Color WrongTimeColor;
	public Color CorrectTimeColor;

	public Text Label;
	public List<Line> Lines;
	public GameObject FlamePrefab;

	public int BurnTime {
		get {
			return _burnTime;
		}
		set {
			_burnTime = value;
			if(_burnTime < 100)
				SetLabel(_burnTime.ToString());
			else
				SetLabel("");

			GetComponent<Image>().color = HasPetard ?
				(_burnTime == 5 ? CorrectTimeColor : WrongTimeColor) : StandardColor;
		}
	}
	public bool Fired { get { return _fired; } }

	int _burnTime;
	bool _fired = false;

	public bool HasFlame
	{
		get {
			return GetComponentInChildren<Slot>().Item != null;
		}
	}

	public bool HasPetard
	{
		get {
			return GetComponentInChildren<Petard>() != null;
		}
	}

	public void SetLabel(string text)
	{
		Label.text = text;
	}

	public void Fire()
	{
		if(_fired)
			return;
		_fired = true;

		for(int i=0; i<Lines.Count; i++)
		{
			Line line = Lines[i];

			Join nextJoin = line.StartJoin == this ? line.EndJoin : line.StartJoin;

			if(nextJoin.BurnTime + line.BurningTime <= BurnTime)
				continue;

			GameObject flame = Instantiate(FlamePrefab);
			flame.transform.SetParent(transform.parent, false);

			Vector3 direction = nextJoin.transform.position - transform.position;
			float distance = 1.0f;

			if(nextJoin.BurnTime < BurnTime + line.BurningTime)
			{
				float timeDiff = BurnTime - nextJoin.BurnTime;
				distance = (line.BurningTime - timeDiff) / (2.0f * line.BurningTime);
			}

			flame.transform.position = transform.position;
			LeanTween.move (flame.gameObject, transform.position + direction * distance, line.BurningTime / 2.0f * distance)
				.setOnComplete( ()=> {
					Destroy(flame);
					nextJoin.Fire();
				});
		}

		Petard petard = GetComponentInChildren<Petard>();
		if(petard != null)
		{
			petard.Fire();
		}
	}

	public void Reset()
	{
		_fired = false;

		if(HasPetard)
			GetComponent<Image>().color = WrongTimeColor;

		Petard petard = GetComponentInChildren<Petard>();
		if(petard != null)
		{
			petard.Reset();
		}
	}

}
