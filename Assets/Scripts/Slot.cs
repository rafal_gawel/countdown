﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{

	public Transform Item {
		get {
			if(transform.childCount > 0)
			{
				return transform.GetChild(0);
			}
			return null;
		}
	}

	#region IDropHandler implementation
	public void OnDrop (PointerEventData eventData)
	{
		if(!Item)
		{
			Flame.Dragging.transform.SetParent(transform);
			LeanTween.move(Flame.Dragging.gameObject, transform.position, 0.1f)
				.setEase(LeanTweenType.easeOutSine);

			Board.Instance.OnChanged();
		}
	}
	#endregion

}
