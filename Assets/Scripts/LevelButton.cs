﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelButton : MonoBehaviour
{

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	void OnClick()
	{
		int levelNumber = transform.GetSiblingIndex();
		GameFlow.Instance.OpenLevel(levelNumber);
	}

}
