﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Flame : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static Flame Dragging;

	Transform _startParent;
	Vector3 _startPosition;

	Transform CanvasTransform {
		get {
			Transform t = transform;

			while(t.GetComponent<Canvas>() == null)
			{
				t = t.parent;
			}

			return t;
		}
	}

	#region IBeginDragHandler implementation
	public void OnBeginDrag (PointerEventData eventData)
	{
		if(Board.Instance.Completed)
			return;

		_startParent = transform.parent;
		_startPosition = transform.position;
		transform.SetParent(CanvasTransform);
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		Dragging = this;
	}
	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		if(Board.Instance.Completed)
			return;

		transform.position = eventData.position;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		if(transform.parent == CanvasTransform)
		{
			transform.SetParent(_startParent);
			LeanTween.move(gameObject, _startPosition, 0.2f)
				.setEase(LeanTweenType.easeOutSine);
		}
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		Dragging = null;
	}

	#endregion
}
