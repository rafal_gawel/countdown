﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlameAnimation : MonoBehaviour
{
	public int FlamesCount;

	public FlameParticle[] FlamePrefabs;

	List<FlameParticle> _flames = new List<FlameParticle>();

	void Start ()
	{
		for(int i=0; i<FlamesCount; i++)
		{
			FlameParticle particle = Instantiate(FlamePrefabs[Random.Range(0, FlamePrefabs.Length)]);
			particle.transform.SetParent(transform, false);
			_flames.Add(particle);
		}
	}

	void Update ()
	{
	
	}
}
