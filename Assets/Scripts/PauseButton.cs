﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseButton : MonoBehaviour
{

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	void OnClick()
	{
		if(!Board.Instance.Completed)
			GameFlow.Instance.OpenLevelSelector();
	}

}
