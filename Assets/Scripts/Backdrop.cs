﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Backdrop : MonoBehaviour
{
	
	public static Backdrop Instance;

	Action _action;

	void Awake ()
	{
		Instance = this;
		GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
		gameObject.SetActive(false);
	}

	public void FadeInOut(Action action)
	{
		gameObject.SetActive(true);
		_action = action;

		Color color = GetComponent<Image>().color;
		color.a = 0;
		GetComponent<Image>().color = color;

		LeanTween.alpha(GetComponent<RectTransform>(), 1.0f, 0.5f)
			.setEase(LeanTweenType.easeOutSine)
			.setOnComplete(CallActionAndFadeOut);
	}

	void CallActionAndFadeOut()
	{
		_action();
		LeanTween.alpha(GetComponent<RectTransform>(), 0.0f, 0.5f)
			.setEase(LeanTweenType.easeInSine)
			.setOnComplete(DisableMe);
	}

	void DisableMe()
	{
		gameObject.SetActive(false);
	}

}
