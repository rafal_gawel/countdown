﻿using UnityEngine;
using System.Collections;

public class LevelSelector : MonoBehaviour
{

	public static LevelSelector Instance;

	void Awake()
	{
		Instance = this;
	}

	void Start ()
	{
		GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}
	
	public void Hide()
	{
		gameObject.SetActive(false);
	}

}
