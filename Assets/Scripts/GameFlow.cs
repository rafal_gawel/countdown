﻿using UnityEngine;
using System.Collections;

public class GameFlow : MonoBehaviour
{

	public static GameFlow Instance;

	void Awake()
	{
		Instance = this;
	}

	public void OpenLevel(int index)
	{
		Backdrop.Instance.FadeInOut(()=>{
			LevelSelector.Instance.Hide();
			GameUI.Instance.Show();
			LevelManager.Instance.HideLevels();
			LevelManager.Instance.ShowLevel(index);
			Board.Instance.OnChanged();
		});
	}

	public void OpenLevelSelector()
	{
		Backdrop.Instance.FadeInOut(()=>{
			LevelManager.Instance.HideLevels();
			GameUI.Instance.Hide();
			LevelSelector.Instance.Show();
		});
	}

}
