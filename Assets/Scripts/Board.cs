﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour
{

	public static Board Instance;

	Join[] _joins;
	Line[] _lines;

	public bool Completed { get; private set; }

	void OnEnable()
	{
		Instance = this;
	}

	void Awake()
	{
		GetComponent<RectTransform>().anchoredPosition = Vector3.zero;

		_joins = GetComponentsInChildren<Join>();
		_lines = GetComponentsInChildren<Line>();
	}

	public void OnChanged()
	{
		for(int i=0; i<_joins.Length; i++)
		{
			_joins[i].BurnTime = _joins[i].HasFlame ? 0 : 100;
		}

		for(int i=0; i<_joins.Length-1; i++)
		{
			for(int j=0; j<_lines.Length; j++)
			{
				Line line = _lines[j];
				if(line.StartJoin.BurnTime > line.EndJoin.BurnTime + line.BurningTime)
					line.StartJoin.BurnTime = line.EndJoin.BurnTime + line.BurningTime;
				if(line.EndJoin.BurnTime > line.StartJoin.BurnTime + line.BurningTime)
					line.EndJoin.BurnTime = line.StartJoin.BurnTime + line.BurningTime;
			}
		}

		bool win = true;
		for(int i=0; i<_joins.Length; i++)
		{
			if(_joins[i].HasPetard && _joins[i].BurnTime != 5)
				win = false;
		}

		if(win)
		{
			Completed = true;
			StartWinAnimation();
		}
	}

	void StartWinAnimation()
	{
		for(int i=0; i<_joins.Length; i++)
		{
			if(_joins[i].HasFlame)
				_joins[i].Fire();
		}

		Invoke ("GoToNextLevel", 3.5f);

		Clock.Instance.StartCounting();
	}

	void GoToNextLevel()
	{
		int nextLevelIndex = (transform.GetSiblingIndex() + 1) % transform.parent.childCount;
		GameFlow.Instance.OpenLevel(nextLevelIndex);
	}

	public void Reset ()
	{
		for(int i=0; i<_joins.Length; i++)
			_joins[i].Reset();

		for(int i=0; i<_lines.Length; i++)
			_lines[i].Reset();

		Completed = false;
	}
}
