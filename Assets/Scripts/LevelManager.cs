﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour
{

	public static LevelManager Instance;
	
	void Awake ()
	{
		Instance = this;

		HideLevels();

		GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
	}

	public void ShowLevel(int level)
	{
		transform.GetChild(level).gameObject.SetActive(true);
		transform.GetChild(level).GetComponent<Board>().Reset();
		GameUI.Instance.Reset();
	}

	public void HideLevels()
	{
		for(int i=0; i<transform.childCount; i++)
			transform.GetChild(i).gameObject.SetActive(false);
	}

}
