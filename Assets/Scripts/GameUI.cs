﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour
{

	public static GameUI Instance;

	StartSlot[] _startSlots;
	
	void Awake ()
	{
		Instance = this;
		_startSlots = GetComponentsInChildren<StartSlot>();
		gameObject.SetActive(false);
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Reset()
	{
		for(int i=0; i<_startSlots.Length; i++)
			_startSlots[i].Reset();
		Clock.Instance.Reset();
	}

}
