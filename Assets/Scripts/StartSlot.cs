﻿using UnityEngine;
using System.Collections;

public class StartSlot : MonoBehaviour
{

	Transform _flame = null;

	void Awake()
	{
		if(transform.childCount > 0)
		{
			_flame = transform.GetChild(0);
		}
	}

	public void Reset()
	{
		if(_flame != null)
		{
			_flame.SetParent(transform, false);
			_flame.localPosition = Vector3.zero;
		}
	}

}
