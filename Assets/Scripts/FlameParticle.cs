﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlameParticle : MonoBehaviour
{
	public float StartXRange; 
	public float YDirection;

	float _lifeTime;
	float _speed;
	Vector3 _direction;
	Vector3 _lastParentPosition;

	void Start ()
	{
		Reset ();
		_lifeTime = Random.Range(0.0f, 1.0f);
		_lastParentPosition = transform.parent.localPosition;
	}

	void FixedUpdate ()
	{
		_lifeTime += Time.deltaTime * 2.0f;

		transform.localPosition += _direction * Time.deltaTime * _speed * 2.0f;
		float s = 1.0f - _lifeTime;
		transform.localScale = new Vector3(s, s, 1);

		Color color = GetComponent<Image>().color;
		float a = 1.0f - _lifeTime*_lifeTime;
		color.a = a;
		GetComponent<Image>().color = color;

		if(_lifeTime >= 1.0f)
			Reset ();
	}

	void LateUpdate()
	{
		transform.localPosition += (_lastParentPosition - transform.parent.position) / transform.parent.lossyScale.x;
		_lastParentPosition = transform.parent.position;
	}

	void Reset()
	{
		_lifeTime -= 1.0f;
		transform.localPosition = new Vector3(0, 0, 0);
		float x = Random.Range(-StartXRange, StartXRange);
		_direction = new Vector3(-x, YDirection);
		_speed = Random.Range(0.6f, 1.0f);
	}
}
