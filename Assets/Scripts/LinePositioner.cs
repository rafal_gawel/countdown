﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Line))]
[ExecuteInEditMode]
public class LinePositioner : MonoBehaviour
{
	void Update ()
	{
		Line line = GetComponent<Line>();

		Join[] joins = FindObjectsOfType<Join>();
		for(int i=0; i<joins.Length; i++)
		{
			joins[i].Lines.Remove(null);
			if(joins[i].Lines.Contains(line))
				joins[i].Lines.Remove(line);
		}

		if(line.StartJoin == null || line.EndJoin == null)
			return;

		RectTransform rectTransform = GetComponent<RectTransform>();
		RectTransform startTransform = line.StartJoin.GetComponent<RectTransform>();
		RectTransform endTransform = line.EndJoin.GetComponent<RectTransform>();

		transform.position = (startTransform.position + endTransform.position) / 2.0f;

		Vector2 size = rectTransform.sizeDelta;
		size.y = (startTransform.anchoredPosition - endTransform.anchoredPosition).magnitude;
		rectTransform.sizeDelta = size;

		float angle = Vector3.Angle(Vector3.up, (startTransform.position - endTransform.position));
		if(startTransform.position.x > endTransform.position.x)
			angle = -angle;
		transform.rotation = Quaternion.Euler(0, 0, angle);

		if(line.Label != null)
		{
			line.Label.transform.rotation = Quaternion.identity;
			line.Label.text = "+"+line.BurningTime.ToString();
		}

		if(!line.StartJoin.Lines.Contains(line))
			line.StartJoin.Lines.Add(line);

		if(!line.EndJoin.Lines.Contains(line))
			line.EndJoin.Lines.Add(line);
	}
}
