﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Line : MonoBehaviour
{
	public Join StartJoin;
	public Join EndJoin;

	public Image StartBurnedImage;
	public Image EndBurnedImage;

	public Text Label;

	public int BurningTime = 2;

	void Update ()
	{
		if(StartJoin != null && EndJoin != null)
		{
			if(StartJoin.Fired)
				StartBurnedImage.fillAmount += Time.deltaTime * 2.0f / BurningTime;
			if(EndJoin.Fired)
				EndBurnedImage.fillAmount += Time.deltaTime * 2.0f / BurningTime;
		}
	}

	public void Reset()
	{
		StartBurnedImage.fillAmount = 0;
		EndBurnedImage.fillAmount = 0;
	}

}
