﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Clock : MonoBehaviour
{

	public static Clock Instance;

	public Image GrayBackground;

	float _time;
	bool _started;
	Text _text;

	void Awake ()
	{
		Instance = this;
		_text = GetComponentInChildren<Text>();
	}
	
	void Update ()
	{
		if(_started)
		{
			_time -= Time.deltaTime * 2.0f;
			if(_time < 0)
				_time = 0;
			int seconds =  Mathf.CeilToInt(_time);
			float fillAmount = 1.0f - Mathf.Repeat(_time, 1.0f);
			GrayBackground.fillAmount = fillAmount;
			_text.text = seconds.ToString();
		}
	}

	public void Reset()
	{
		_time = 5.0f;
		_started = false;
		GrayBackground.fillAmount = 0.0f;
		_text.text = "5";
	}

	public void StartCounting()
	{
		_started = true;
	}
}
