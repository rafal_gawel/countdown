﻿using UnityEngine;
using System.Collections;

public class Petard : MonoBehaviour
{

	public GameObject Flame;

	Vector3 _startPosition;

	void Awake()
	{
		_startPosition = transform.localPosition;
	}

	public void Fire()
	{
		LeanTween.move(gameObject, transform.position + new Vector3(0, 1000), 1.0f)
			.setEase(LeanTweenType.easeInSine);
		Flame.SetActive(true);
	}

	public void Reset()
	{
		LeanTween.cancel(gameObject, false);
		Flame.SetActive(false);
		transform.localPosition = _startPosition;
	}

}
